import {RouterModule,Routes} from '@angular/router';
import { HomeComponent} from './components/home/home.component';
import {FavoritosListComponent} from './components/favoritos-list/favoritos-list.component';
import {RegistroComponent} from './components/registro/registro.component';
import {InsertarComponent} from './components/insertar/insertar.component';
import {EditarComponent} from './components/editar/editar.component';

const APP_ROUTES: Routes = [
  //{path:'home',component: HomeComponent},
  {path:'favoritos',component: FavoritosListComponent},
  {path:'registro',component: RegistroComponent},
  {path:'insertar',component: InsertarComponent},
  {path:'editar/:id',component: EditarComponent},
  {path: '**', pathMatch: 'full', redirectTo:'favoritos'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES); //Por ahora quitaremos el has { useHash:true}
