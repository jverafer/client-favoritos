import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // <-- import required BrowserAnimationsModule
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import {APP_ROUTING} from './app.routes';
// Import your library
import { CollapsibleModule } from 'angular2-collapsible'; // <-- import the module


import{FavoritoService} from "./servicios/favoritoService";

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FavoritosListComponent } from './components/favoritos-list/favoritos-list.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { RegistroComponent } from './components/registro/registro.component';
import { InsertarComponent } from './components/insertar/insertar.component';
import { EditarComponent } from './components/editar/editar.component';
import { BorrarComponent } from './components/borrar/borrar.component';
import { PruebaTablaComponent } from './components/prueba-tabla/prueba-tabla.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FavoritosListComponent,
    NavbarComponent,
    RegistroComponent,
    InsertarComponent,
    EditarComponent,
    BorrarComponent,
    PruebaTablaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    APP_ROUTING,
    CollapsibleModule,
    BrowserAnimationsModule 
  ],
  providers: [
    FavoritoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
