import { Component, OnInit } from '@angular/core';
import {FavoritoService} from '../../servicios/favoritoService';
import{Favorito} from '../../models/favoritoModel';

@Component({
  selector: 'app-favoritos-list',
  templateUrl: './favoritos-list.component.html',
  styleUrls: ['./favoritos-list.component.css']
})
export class FavoritosListComponent implements OnInit {
  public title:string;
  public favoritos: Favorito[];
  public errorMessage;
  constructor(
    private _favoritoService: FavoritoService
  ) {
    this.title = 'Listado de componentes';
  }
  ngOnInit() {
  }
  verLista(){
  //console.log('Favoritos Cargando');
  this._favoritoService.getFavoritos().subscribe(
    result =>{
      //console.log(result);
      this.favoritos = result.favoritos;
      if(!this.favoritos){
        alert('Error en el servidor');
      }
    },error =>{
      this.errorMessage = <any>error;
      if(this.errorMessage != null){
        //console.log(this.errorMessage);
        alert('Error en la petición');
      }
    }
  );
}

}
