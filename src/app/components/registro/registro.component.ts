import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {FavoritoService} from '../../servicios/favoritoService';
import{Favorito} from '../../models/favoritoModel';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  public ind:boolean = false;
  public title:string;
  public favorito: Favorito;
  public errorMessage;
  public id:string;
  constructor(
    private _favoritoService: FavoritoService,
    private _route: ActivatedRoute,
    private _router:Router
  ) {
    this.title = 'Muestra un registro';
  }

  ngOnInit() {
  }
  verFav(ids:string){
  //console.log('Favoritos Cargando');
  this._favoritoService.getFavorito(ids).subscribe(
    result =>{
      //console.log(result);
      this.favorito = result.favorito;
      if(!this.favorito){
        alert('Error en el servidor');
      }
    },error =>{
      this.errorMessage = <any>error;
      if(this.errorMessage != null){
        //console.log(this.errorMessage);
        alert('Error en la petición');
      }
    });
  }
  public delComp(id:string){
    //console.log(this.favorito);
    this._route.params.forEach((params:Params) =>{
      //let id = params['id'];
      //console.log(id);
    this._favoritoService.delFavorito(id, this.favorito).subscribe(
      response => {
          this.favorito = response.favorito;
        alert('Se ha borrado correctamente el registro');
        //this._router.navigate(['/registro']);
         window.location.reload();
      },
      error =>{
        this.errorMessage = <any>error;
        if(this.errorMessage != null){
          //console.log(this.errorMessage);
          alert('Error en la petición');
        }
      }
    );
  });
  }
}
