import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {FavoritoService} from '../../servicios/favoritoService';
import {Favorito} from '../../models/favoritoModel';

@Component({
  selector: 'app-insertar',
  templateUrl: './insertar.component.html',
  styleUrls: ['./insertar.component.css']
})
export class InsertarComponent implements OnInit {
  public titulo: string;
  public description: string;
  public favorito:Favorito;
  public errorMessage: any;

  constructor(
    private _favoritoService:FavoritoService,
    private _route:ActivatedRoute,
    private _router: Router
  ) {
      this.titulo = "INSERTAR";
      this.description =  "Aquíinsertaremos registro";
  }

  ngOnInit() {
    this.favorito = new Favorito("","","","");
    //console.log(this.favorito);
  }
  onSubmit(){
    console.log(this.favorito);
    this._favoritoService.addFavorito(this.favorito).subscribe(
      result => {
        //console.log(result.toString.prototype+'----------------  ');
          this.favorito = result.favorito;
          //console.log(this.favorito);
          this._router.navigate(['/favoritos']);
          alert('Se ha insertado correctamente el registro');
      },error =>{
        this.errorMessage = <any>error;
        if(this.errorMessage != null){
          //console.log(this.errorMessage);
          alert('Error en la petición');
        }
      }
    );
  }
}
