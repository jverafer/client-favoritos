import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute,Params} from '@angular/router';
import {FavoritoService} from '../../servicios/favoritoService';
import {Favorito} from '../../models/favoritoModel';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
  public titulo: string;
  public description: string;
  public favorito:Favorito;
  public errorMessage: any;

  constructor(
    private _favoritoService:FavoritoService,
    private _route: ActivatedRoute,
    private _router:Router
  ) {
    this.titulo = "Editar";
    this.description =  "Aquíinsertaremos registro";
  }

  ngOnInit() {
    this.favorito = new Favorito("","","","");
    //console.log(this.favorito);
    this.getFavorito();
  }
  getFavorito(){
    this._route.params.forEach((params:Params) => {
      let id = params['id'];
      this._favoritoService.getFavorito(id).subscribe(response => {
      this.favorito = response.favorito;
      },error =>{ this.errorMessage = <any>error;
        if(this.errorMessage != null){
          //console.log(this.errorMessage);
          alert('Error en la petición');
        }
      });
    });
  }
  public onSubmit(){
    //console.log(this.favorito);
    this._route.params.forEach((params:Params) =>{
      let id = params['id'];
    this._favoritoService.editFavorito(id, this.favorito).subscribe(
      response => {
          this.favorito = response.favorito;
          alert('Se ha actualizado correctamente el registro');
        this._router.navigate(['/registro']);
      },
      error =>{
        this.errorMessage = <any>error;
        if(this.errorMessage != null){
          //console.log(this.errorMessage);
          alert('Error en la petición');
        }
      }
    );
  });
  }
}
